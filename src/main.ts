import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, //ignors the additional data key according to dto data keys
      forbidNonWhitelisted: true, //stops the request and error the additional data key which is not in dto
      transform: true, //automatically transform payloads(body of request) to be objects typed according to their DTO classes.
    }),
  );
  await app.listen(3000);
}
bootstrap();
